const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const cartController = require("../controllers/cartController");

const auth = require("../auth");

// checkout single product
router.post("/checkoutSingleProduct", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.userCheckoutSingle(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

// remove ALL PRODUCTS 
router.delete("/emptyCart", auth.verify, async (req, res) => {
  try {
    const userId = auth.decode(req.headers.authorization).id;
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    const resultFromController = await cartController.emptyCartByUserId(userId, isAdmin);
    res.send(resultFromController);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

// remove single product
router.post("/removeProduct", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.removeProductFromCart(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

// update product quantity
router.post("/amountRemove", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.updateCartQuantity(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

// add multiple products
router.post("/addCart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});


/*==========[STRETCH GOALS]=============*/


// route for retrieving authenticated user's cart 
router.get("/viewCart/:cartId", (req, res) => {
	cartController.retrieveCart(req.params).then(resultFromController => res.send(resultFromController));
});

router.get('/view/userscart', (req, res) => {
  const data = {
  	userId: auth.decode(req.headers.authorization).id,
  }
  cartController.findCartByUserId(data).then(resultFromController => res.send(resultFromController));
});
// ++++++++++++++++++++++++++++++++++++

// route for retrieving ALL carts (ADMIN only)
router.get("/all/createdcarts", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.retrieveAllCart(data).then(resultFromController => res.send(resultFromController));
});

// route for changing the product's quantity inside a cart || add/remove product(s)
router.patch("/changeQuantity/:cartId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.changeQuantity(req.params, data).then(resultFromController => res.send(resultFromController));
});

module.exports = router; 



