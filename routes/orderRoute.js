const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

const auth = require("../auth");


// route for user checkout (creating order)
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		cartId: req.body.cartId
	}
	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});


/*==========[STRETCH GOALS]=============*/


// route for retrieving authenticated user's order
router.get("/:orderId", (req, res) => {
	orderController.retrieveOrder(req.params).then(resultFromController => res.send(resultFromController));
});






module.exports = router; 



