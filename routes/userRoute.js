const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

// Route checkEmailExist
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for user authentication (login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for retrieving user details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// route for setting user as admin (ADMIN only)
router.patch("/setAdmin/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUserToAdmin(req.params, data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
