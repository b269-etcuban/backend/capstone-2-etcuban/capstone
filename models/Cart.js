const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Product ID is required"]
		},
		name: {
			type: String,
			required: [true, "Product Name is required"]
		},
		description: {
			type: String,
			required: [true, "Product description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		quantity: {
			type: Number,
			default: 1
		},
		subTotal: {
			type: Number
		}
	}],
	totalAmount: {
		type: Number,
		// required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Cart", cartSchema);







