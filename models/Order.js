const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	cartId: {
		type: String,
		default: null
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			name: {
				type: String,
				required: [true, "Product Name is required"]
			},
			description: {
				type: String,
				required: [true, "Product description is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			quantity: {
				type: Number,
				default: 1
			},
			subTotal: {
				type: Number
			}
		}
	],
	totalAmount: {
		type: Number
	},
	modifiedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);




