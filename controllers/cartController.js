const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");

// checkoutsingle product
module.exports.userCheckoutSingle = async (userId, isAdmin, productId, quantity) => {
  if (isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    try {
      let cart = await Cart.findOne({ userId });
      const selectedProduct = await Product.findById(productId);
      const amountingPrice = selectedProduct.price * quantity;
      
      if (!cart) {
        cart = new Cart({
          userId,
          products: [{
            productId: selectedProduct._id,
            name: selectedProduct.name,
            description: selectedProduct.description,
            price: selectedProduct.price,
            quantity,
            subTotal: amountingPrice
          }],
          totalAmount: amountingPrice
        });
      } else if (quantity > 0) {
        const index = cart.products.findIndex(product => product.productId.toString() === productId);

        if (index !== -1) {
          cart.products[index].quantity += Number(quantity);
          cart.products[index].subTotal = cart.products[index].quantity * cart.products[index].price;
        } else {
          cart.products.push({
            productId: selectedProduct._id,
            name: selectedProduct.name,
            description: selectedProduct.description,
            price: selectedProduct.price,
            quantity: Number(quantity),
            subTotal: amountingPrice
          });
        }
        cart.totalAmount = cart.products.reduce((total, product) => total + product.subTotal, 0);
      } else {
        cart.products = [];
        cart.totalAmount = 0;
      }

      await cart.save();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
};

// remove ALL PRODUCTS 
module.exports.emptyCartByUserId = async (userId, isAdmin) => {
  if (isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    try {
      const cart = await Cart.findOne({ userId });

      if (!cart) {
        return false; 
      } else {
        cart.products = [];
        cart.totalAmount = 0;

        await cart.save();
        return true; 
      }
    } catch (error) {
      console.log(error);
      return Promise.reject(error);
    }
  }
};


// remove a single product
module.exports.removeProductFromCart = async (userId, isAdmin, productId) => {
	if (isAdmin) {
		return Promise.resolve({value: `This feature is not available to you.`});
	} else {
		try {
			let cart = await Cart.findOne({ userId });
			
			if (!cart) {
				return Promise.resolve({value: `Cart not found.`});
			} else {
				const index = cart.products.findIndex(product => product.productId.toString() === productId);
				
				if (index !== -1) { 
					const removedProduct = cart.products.splice(index, 1);
					cart.totalAmount -= removedProduct[0].subTotal;
					
					return cart.save().then(() => {
						return true;
					});
				} else { 
					return Promise.resolve({value: `Product not found in cart.`});
				}
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	}
};

// update a product quantity
module.exports.updateCartQuantity = async (userId, isAdmin, productId, newQuantity) => {
	if (isAdmin) {
		return Promise.resolve({value: `This feature is not available to you.`});
	} else {
		try {
			let cart = await Cart.findOne({ userId });
			
			if (!cart) {
				return Promise.resolve({value: `Cart not found for user.`});
			} else { 
				const index = cart.products.findIndex(product => product.productId.toString() === productId);
				if (index !== -1) { 
					cart.products[index].quantity = newQuantity;
					cart.products[index].subTotal = cart.products[index].quantity * cart.products[index].price;
					
					cart.totalAmount = cart.products.reduce((total, product) => total + product.subTotal, 0);
					
					await cart.save();
					
					return true;
				} else { 
					return Promise.resolve({value: `Product not found in cart.`});
				}
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	}
};

// add multiple products
module.exports.addToCart = async (data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		console.log(productsArray);
		const productSpec = [];
		let newCart = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct;
			
			newCart.products.push(pushToProductsArray);
		}

		newCart = new Cart({
			userId: data.userId,
			products: newCart.products,
			totalAmount: currentTotalAmount
		});

		const cart = await newCart.save();

		if (cart) {
			return true;
		} else {
			return false;
		}
	}
};

/*==========[STRETCH GOALS]=============*/

// retrieve authenticated user's cart
module.exports.retrieveCart = (reqParams) => {
	return Cart.findById(reqParams.cartId).then(result => {
		return result;
	});
};

module.exports.findCartByUserId = (data) => {
	return Cart.find({userId: data.userId}).then(result => {
		return result;
	});
};
// ++++++++++++++++++++++++++++++++++++

// retrieve ALL carts (ADMIN only)
module.exports.retrieveAllCart = (data) => {
	if (data.isAdmin) {
		return Cart.find({}).then(result => {return result; });
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return value;
	});
};

// change product's quantity inside a cart || add/remove product(s)
module.exports.changeQuantity = async (reqParams, data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		const productSpec = [];
		let newQuantity = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct; // accumulate the total
			newQuantity = {
				userId: data.userId,
				products: productSpec,
				totalAmount: currentTotalAmount
			};

			newQuantity.products.push(pushToProductsArray);
		}

		return Cart.findByIdAndUpdate(reqParams.cartId, newQuantity).then((cart, error) => {
			if (error) {
				return false;
			} else {
				let cartUpdated = `The cart has been successfully updated. You new current total is ₱${currentTotalAmount}.`
				return cartUpdated;
			};
		});
	}
};


