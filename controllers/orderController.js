const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const Cart = require("../models/Cart");


// user checkout (creating order)
module.exports.createOrder = async (data) => {
  if (data.isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    const selectedCart = await Cart.findById({ _id: data.cartId });
    console.log(selectedCart);

    const productSpecs = selectedCart.products;
    console.log(productSpecs);

    const newOrder = new Order({
      userId: selectedCart.userId,
      cartId: data.cartId,
      products: productSpecs,
      totalAmount: selectedCart.totalAmount,
    });

    // Empty the cart and set totalAmount to 0
    selectedCart.products = [];
    selectedCart.totalAmount = 0;
    await selectedCart.save();

    return newOrder.save().then((order, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  }
};


/*==========[STRETCH GOALS]=============*/

// retrieve authenticated user's order
module.exports.retrieveOrder = (reqParams) => {
	return Order.findById(reqParams.orderId).then(result => {
		return result;
	});
};






