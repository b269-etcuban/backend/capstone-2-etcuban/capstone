const Product = require("../models/Product");

// creating product (ADMIN ONLY)
module.exports.createProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product ({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price 
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let notAdminMessage = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return notAdminMessage.then((value) => {
		return {value};
	});
	
};

// retrieving all ACTIVE products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// retrieve a single specific product
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// updating product information (ADMIN only)
module.exports.updateProductInfo = (reqParams, data) => {
	if (data.isAdmin) {
		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
};

// archiving product (ADMIN only)
module.exports.archiveProduct = (reqParams, data) => {
	if (data.isAdmin) {
		let archivedProduct = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
};

// reactivating product (ADMIN only)
module.exports.reactivateProduct = (reqParams, data) => {
	if (data.isAdmin) {
		let reactivatedProduct = {
			isActive: true
		};
		return Product.findByIdAndUpdate(reqParams.productId, reactivatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
};

// getting all products for ADMIN only
module.exports.retrieveAllProducts = (data) => {
	if (data.isAdmin) {
		return Product.find({}).then(result => {
			return result; 
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return value;
	});
};


