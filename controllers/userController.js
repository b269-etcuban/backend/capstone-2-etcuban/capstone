const User = require("../models/User");
const Product = require("../models/Product"); 
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

// to register a user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// log in 
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false;
		} else if (reqBody.password == null) {
			return false;
		} else {
			const isPasswordRight = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordRight) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			};
		};
	});
};

// retrieving user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	});
};

// setting user to admin (ADMIN only)
module.exports.setUserToAdmin = (reqParams, data) => {
	if (data.isAdmin) {
		let userToAdmin = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(reqParams.userId, userToAdmin).then((user, error) => {
			if (error) {
				return false;
			} else {
				let requestAccepted = `User successfully updated to Admin.`;
				return requestAccepted;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
}